package ru.remezov.animal;

public interface Flyable {

    default void canFly() {
        System.out.println("I can fly!");
    }
}
