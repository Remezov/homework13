package ru.remezov.animal;

public abstract class Animal {
    public abstract void getName();
    protected String name;

    public Animal(String name) {
        this.name = name;
    }
}
