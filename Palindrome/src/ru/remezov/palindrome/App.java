package ru.remezov.palindrome;

import java.util.Scanner;

public class App {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.println("Enter a word, a number or a sentence (no punctuation)");
        String word = s.nextLine();
        String word0 = word.replaceAll(" ","");
        String word1 = word0.toLowerCase();
        int length = word1.length();
        for (int i = 0; i <= ((length-1)/2); i++) {
            if (word1.charAt(0+i) == word1.charAt(length-1-i)) {
                if (i == ((length-1)/2)) {
                    System.out.println("It's palindrome.");
                }
                continue;
            }
            else {
                System.out.println("It's not palindrome.");
                break;
            }
        }
    }
}
